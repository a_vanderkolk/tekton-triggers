{{/*
Expand the name of the chart.
*/}}
{{- define "tekton-triggers.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "tekton-triggers.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "tekton-triggers.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "tekton-triggers.labels" -}}
helm.sh/chart: {{ include "tekton-triggers.chart" . }}
{{ include "tekton-triggers.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "tekton-triggers.selectorLabels" -}}
app.kubernetes.io/name: {{ include "tekton-triggers.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "tekton-triggers.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "tekton-triggers.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "java.trigger.binding" -}}
{{- $tektonPipelines := index .Values "tekton-pipelines" -}}
{{- $triggerTemplatesJava := .Values.triggerTemplates.build.java -}}
{{- $ttJavaVariables := $triggerTemplatesJava.variables -}}
{{- $pipelineJava := $tektonPipelines.pipelines.build.java -}}
{{- $pipelineJavaVariables := $pipelineJava.variables -}}
- name: {{ $pipelineJavaVariables.sourceCode.imageName }}
  value: {{ printf "%s/$(tt.params.%s)" .Values.global.configuration.imageRepository.host $ttJavaVariables.project.fullName }}
- name: {{ $pipelineJavaVariables.sourceCode.contextDir }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.sourceCode.contextDir}}
- name: {{ $pipelineJavaVariables.sourceCode.repoUrl }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.sourceCode.repositoryUrl }}
- name: {{ $pipelineJavaVariables.sourceCode.repoRevision }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.sourceCode.repositoryRef }} 
- name: {{ $pipelineJavaVariables.sourceCode.dockerfileLocation }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.sourceCode.dockerfileLocation }} 
- name: {{ $pipelineJavaVariables.infrastructure.updateHelmChart }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.infrastructure.updateHelmChart }} 
- name: {{ $pipelineJavaVariables.infrastructure.repoUrl }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.infrastructure.repositoryUrl }}
- name: {{ $pipelineJavaVariables.infrastructure.repoRevision }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.infrastructure.repositoryRef }}
- name: {{ $pipelineJavaVariables.infrastructure.yamlPaths.image }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.infrastructure.yamlPaths.image }}
- name: {{ $pipelineJavaVariables.infrastructure.yamlPaths.sha }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.infrastructure.yamlPaths.sha }}
- name: {{ $pipelineJavaVariables.infrastructure.deleteExistingInfraBranch }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.infrastructure.deleteExistingInfraBranch }}
- name: {{ $pipelineJavaVariables.project.giteaUrl }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.project.giteaUrl }}
- name: {{ $pipelineJavaVariables.project.fullName }}
  value: {{ printf "$(tt.params.%s)" $ttJavaVariables.project.fullName }}
# - name: {{ $pipelineJavaVariables.project.fullName }}
#   value: {{ printf "$(tt.params.%s)" $ttJavaVariables.project.safeName }}
{{- end -}}

{{- define "angular.trigger.binding" -}}
{{- $tektonPipelines := index .Values "tekton-pipelines" -}}
{{- $triggerTemplatesAngular := .Values.triggerTemplates.build.angular -}}
{{- $ttAngularVariables := $triggerTemplatesAngular.variables -}}
{{- $pipelineAngular := $tektonPipelines.pipelines.build.angular -}}
{{- $pipelineAngularVariables := $pipelineAngular.variables -}}
- name: {{ $pipelineAngularVariables.project.giteaUrl }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.project.giteaUrl }}
- name: {{ $pipelineAngularVariables.project.fullName }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.project.fullName }}
- name: {{ $pipelineAngularVariables.sourceCode.imageName }}
  value: {{ printf "%s/$(tt.params.%s)" .Values.global.configuration.imageRepository.host $ttAngularVariables.project.fullName }}
- name: {{ $pipelineAngularVariables.sourceCode.repoUrl }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.sourceCode.repositoryUrl }}
- name: {{ $pipelineAngularVariables.sourceCode.repoRevision }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.sourceCode.repositoryRef }} 
- name: {{ $pipelineAngularVariables.sourceCode.contextDir }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.sourceCode.contextDir}}
- name: {{ $pipelineAngularVariables.sourceCode.dockerfileLocation }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.sourceCode.dockerfileLocation }} 
- name: {{ $pipelineAngularVariables.infrastructure.updateHelmChart }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.infrastructure.updateHelmChart }} 
- name: {{ $pipelineAngularVariables.infrastructure.repoUrl }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.infrastructure.repositoryUrl }}
- name: {{ $pipelineAngularVariables.infrastructure.repoRevision }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.infrastructure.repositoryRef }}
- name: {{ $pipelineAngularVariables.infrastructure.yamlPaths.image }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.infrastructure.yamlPaths.image }}
- name: {{ $pipelineAngularVariables.infrastructure.yamlPaths.sha }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.infrastructure.yamlPaths.sha }}
- name: {{ $pipelineAngularVariables.infrastructure.deleteExistingInfraBranch }}
  value: {{ printf "$(tt.params.%s)" $ttAngularVariables.infrastructure.deleteExistingInfraBranch }}
# - name: {{ $pipelineAngularVariables.project.fullName }}
#   value: {{ printf "$(tt.params.%s)" $ttAngularVariables.project.safeName }}
{{- end -}}